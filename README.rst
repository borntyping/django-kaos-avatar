==================
django-kaos-avatar
==================

Extends the ``django-kaos application to add user, player and team avatars.

Usage
=====

Images used take this order of priority:
	
- An uploaded image if one exists for the object
- A gravatar image, fetched with the users email (only for ``User`` and ``Player`` objects, never shown for anonymous players)
- A gravatar image generated from the objects name or email

To use the template tags in a template:

::
	
    {% load markup kaos kaos-avatars %}

Show an url/image for a user/team/player:

::
	
    {% user_avatar user %}
    {% user_avatar_image user %}
    
    {% player_avatar player %}
    {% player_avatar_image player %}
    
    {% team_avatar team %}
    {% team_avatar_image team %}

Changing avatars
----------------

Users can change their user and player avatars from the ``change_avatar`` view, at ``/kaos/avatar/`` if the default urls are used.

Team avatars must be set though the admin interface.

Requirements
============

``django-kaos-avatar`` requires the following python packages:

- ``django-kaos``
- ``PIL`` (python-imaging)

Installation
============


From source:

::

	pip install PIL
	git clone https://github.com/borntyping/django-kaos-avatar.git
	cd django-kaos-avatar
	python setup.py install

In ``settings.py``:

::

	INSTALLED_APPS = (
		...
		'kaos_avatar',
	)

In ``urls.py``:

::
	
	urlpatterns = patterns('',
		...
		url(r'^kaos/avatar/', include('kaos_avatar.urls')),
	)

``tastypie`` is only required if you wish to provide an api.
