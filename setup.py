#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name             = 'kaos-avatar',
    version          = '1.0',
    
    author           = 'Samuel Clements',
    author_email     = 'samuel@borntyping.co.uk',
    url              = 'https://github.com/ziaix/kaos-extensions',
    description      = 'An extension to the kaos application that adds user, player and team avatars.',    
    long_description = open('README.rst').read(),
    
    packages         = find_packages(exclude=['migrations']),
    install_requires = ['django>=1.4', 'PIL', 'django-kaos',],
)
