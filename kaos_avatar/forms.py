"""	Forms for handling avatars """

from django import forms
from kaos_avatar.models import PlayerAvatar

class AvatarChangeForm (forms.Form):
	avatar = forms.ImageField(
		help_text="The new avatar to use.")
	
	def save (self, cls, key):
		new_avatar = self.cleaned_data['avatar']
		try:
			avatar = cls.objects.get(obj=key)
			avatar.avatar = new_avatar
		except cls.DoesNotExist:
			avatar = cls(obj=key, avatar=new_avatar)
		avatar.save()

class PlayerAvatarChangeForm (AvatarChangeForm):
	class PlayerChoiceField(forms.ModelChoiceField):
		def label_from_instance(self, obj):
			return "{}, playing as {}".format(obj.game.title, obj.name())
	
	def __init__ (self, queryset, *args, **kwargs):
		super(PlayerAvatarChangeForm, self).__init__(*args, **kwargs)
		
		player_field = self.PlayerChoiceField(queryset=queryset, empty_label=None,
			help_text="The player avatar to change.")
			
		if queryset.count() == 0:
			player_field.help_text = "You have no player avatars to change."
			player_field.empty_label = "----------"
			player_field.widget.attrs['readonly'] = True
		
		self.fields['player'] = player_field
		self.fields['avatar'].help_text = "The new avatar to use for the selected player."
		
	def save (self):
		super(PlayerAvatarChangeForm, self).save(PlayerAvatar, self.cleaned_data['player'])
