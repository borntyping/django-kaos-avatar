from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('kaos_avatar.views',
	url(r'^clear/(?P<slug>[\w-]+)/', 'clear_player_avatar', name='clear_player_avatar'),
	url(r'^clear/', 'clear_user_avatar', name='clear_user_avatar'),
	
	# Homepage
	url(r'^$', 'change_avatar', name='change_avatar'),
)
