""" Avatar models and admin """

from django.db import models
from django.contrib import admin

from kaos.models import User, Team, Player

class CommonAvatar (models.Model):
	"""	An abstract model with an avatar field """
	
	class Meta:
		abstract = True
		
	# This will be defined by the subclasses
	# obj = models.OneToOneField(...)
	
	# Files are uploaded to `avatars/<obj class>/<obj id>.<ext>`,
	# where obj is the object the avatar is attached to
	avatar = models.ImageField(
		upload_to=lambda obj, filename: "avatars/{}/{}.{}".format(
			obj._meta.verbose_name.split()[0],
			obj.id,
			filename.split('.')[-1]
		),
		help_text="The object's avatar.")

class CommonAvatarAdmin (admin.ModelAdmin):
	"""	An Admin model to manage subclasses of CommonAvatar """
	list_display = ('obj', 'avatar')
	fieldsets = [
		('Avatar', {'fields': ['obj', 'avatar']}),
	]

for cls in (User, Team, Player):
	# The name of the new class to create
	name = "{cls}Avatar".format(cls=cls.__name__)
	
	# The attributes of the new class
	d = {'obj': models.OneToOneField(
		cls,
		related_name='avatar',
		verbose_name=cls.__name__
	), '__module__': CommonAvatar.__module__}
	
	# Create the new class in the module namespace
	model = type(name, (CommonAvatar,), d)
	
	# Register the class with the admin interface
	admin.site.register(model, CommonAvatarAdmin)
	
	# Put the model into the module namespace
	globals()[name] = model
