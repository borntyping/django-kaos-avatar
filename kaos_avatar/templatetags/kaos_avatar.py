"""	Template tags for user, team and player avatars """

import urllib

from django import template
from django.utils.safestring import mark_safe
from django.utils.hashcompat import md5_constructor

from ..models import UserAvatar, PlayerAvatar, TeamAvatar
from ..settings import *

register = template.Library()
	
def gravatar_url (email, default=AVATAR_DEFAULT_IMAGE, size=AVATAR_DEFAULT_SIZE, force=False, **kwargs):
	"""
	Get the url for a gravatar image.
	
	:Parameters:
		- `email` (str): The email to fetch the url for
	
	:Keywords:
		- `default` (str): See `AVATAR_DEFAULT_IMAGE`
		- `size` (int):    The size of the image to fetch
		- `force` (str):   'y' to force use of the default image
	"""
	
	# Prepare options
	options = {'default': default, 'size': size}
	if force: options['f'] = 'y'
	
	# Prepare url
	options = urllib.urlencode(options)
	md5 = md5_constructor(email).hexdigest()
	
	return "{}avatar/{}.jpg?{}".format(AVATAR_GRAVATAR_URL, md5, options)

@register.simple_tag
def user_avatar (user, **kwargs):
	"""
	Return an avatar url for a user.
	
	All keywords are passed on to `gravatar_url()` if it is called.
	
	:Parameters:
		- `user` (User): The user to fetch an avatar for
		- `size` (int):  See `gravatar_url`
		- `force` (str): See `gravatar_url`
	"""
	try:
		return UserAvatar.objects.get(obj=user).avatar.url
	except UserAvatar.DoesNotExist:
		return gravatar_url(user.email, **kwargs)

@register.inclusion_tag(['kaos/avatar/user_avatar.html', 'kaos/avatar/avatar.html'])
def user_avatar_image (user, **kwargs):
	"""
	Return an image tag for an url
	"""
	kwargs['size'] = kwargs.pop('size', AVATAR_DEFAULT_SIZE)
	kwargs['url'] = user_avatar(user, **kwargs)
	return kwargs

@register.simple_tag
def player_avatar (player, **kwargs):
	"""
	Return an avatar url for a player.
	
	All keywords are passed on to `gravatar_url()` if it is called.
	
	:Parameters:
		- `player` (Player): The player to fetch an avatar for
	"""
	try:
		return PlayerAvatar.objects.get(obj=player).avatar.url
	except PlayerAvatar.DoesNotExist:
		# Do not show the user avatar if the player is using a pseudonym.
		if player.is_anon():
			return gravatar_url(player.pseudonym, f=True, **kwargs)
		else:
			return gravatar_url(player.user.email, **kwargs)

@register.inclusion_tag(['kaos/avatar/player_avatar.html', 'kaos/avatar/avatar.html'])
def player_avatar_image (player, **kwargs):
	"""
	Return an image tag for an url
	"""
	kwargs['size'] = kwargs.pop('size', AVATAR_DEFAULT_SIZE)
	kwargs['url'] = player_avatar(player, **kwargs)
	return kwargs

@register.simple_tag
def team_avatar (team, **kwargs):
	"""
	Return an avatar url for a team.
	
	Uses a default gravatar if the team has no avatar.
	
	:Parameters:
		- team (Team): The team to fetch an avatar for
	"""
	try:
		url = TeamAvatar.objects.get(obj=team).avatar.url
	except TeamAvatar.DoesNotExist:
		url = gravatar_url(team.name, f=True, **kwargs)
	return url

@register.inclusion_tag(['kaos/avatar/team_avatar.html', 'kaos/avatar/avatar.html'])
def team_avatar_image (team, **kwargs):
	"""
	Return an image tag for an url
	"""
	kwargs['size'] = kwargs.pop('size', AVATAR_DEFAULT_SIZE)
	kwargs['url'] = team_avatar(team, **kwargs)
	return kwargs
