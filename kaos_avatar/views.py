"""	Views for avatars """

from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from django.shortcuts import render, redirect

from kaos.models import Game, Player
from kaos_avatar.models import UserAvatar, PlayerAvatar
from kaos_avatar.forms import AvatarChangeForm, PlayerAvatarChangeForm

@login_required
def change_avatar (request):
	"""	Allow a user to change their player or user avatars """
	queryset = Player.objects.filter(user=request.user, game__end_time__gt=datetime.today())
	if request.method == 'POST':
		user_form = AvatarChangeForm(request.POST, request.FILES, prefix="user")
		player_form = PlayerAvatarChangeForm(queryset, request.POST, request.FILES, prefix="player")
		
		if user_form.is_valid():
			user_form.save(UserAvatar, request.user)
		
		if player_form.is_valid():
			player_form.save()
		
		if user_form.is_valid() or player_form.is_valid():
			return redirect(reverse('change_avatar'))
	else:
		user_form = AvatarChangeForm(prefix="user")
		player_form = PlayerAvatarChangeForm(queryset, prefix="player")
	
	return render(request, 'kaos/avatar/change_avatar.html', {
		'user_form': user_form,
		'player_form': player_form,
		'players': queryset,
	})

@login_required
def clear_user_avatar (request):
	try:
		UserAvatar.objects.get(obj=request.user).delete()
	except UserAvatar.DoesNotExist:
		pass
	return redirect(reverse('change_avatar'))

@login_required
def clear_player_avatar (request, slug):
	try:
		player = Player.objects.get(user=request.user, game__slug=slug)
		PlayerAvatar.objects.get(obj=player).delete()
	except ObjectDoesNotExist:
		pass
	return redirect(reverse('change_avatar'))
