""" Settings for the kaos-avatar package """

from django.conf import settings

#: The default size of image to use
AVATAR_DEFAULT_SIZE  = getattr(settings, 'AVATAR_DEFAULT_SIZE', 40)

#: The default image to use when fetching a gravatar
#: One of ['404','mm','identicon','monsterid','wavatar','retro'] or an url
AVATAR_DEFAULT_IMAGE = getattr(settings, 'AVATAR_DEFAULT_IMAGE', 'mm')

#: The url to make gravatar requests to
AVATAR_GRAVATAR_URL  = getattr(settings, 'AVATAR_GRAVATAR_URL', 'http://www.gravatar.com/')
	
#: The default color to use for teams
AVATAR_DEFAULT_TEAM_COLOR = getattr(settings, 'AVATAR_DEFAULT_TEAM_COLOR', '#000000')
